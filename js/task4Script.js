$(document).ready(function () {  
    $.get("http://www.json-generator.com/api/json/get/crfAiwPYmW?indent=2", function (data, status) {

        data.forEach(function (item, i, data) {

            var bookWrapper = $('<div class="col-md-4"></div>');
            var book = $('<div class="book"></div>');
            book.css('background', 'url(' + item.image + ') no-repeat');
            book.css('background-size', '100%');           
         
            var bookType = $('<div></div>').addClass(item.type);
            bookType.hide();

            var bookHeader = $('<div class="book-header"></div>').text(item.name);
            var bookAuthor = $('<div class="book-author"></div>').text(item.author);
            var bookIntro = $('<div class="book-intro"></div>').text(item.intro);
            var bookPriceWrapper = $('<div class="book-price-wrap"></div>');
            var bookPrice = $('<i class="book-price"></i>').text(item.price);
            

            var bookBuyButton =    
                '<div class="book-buy-button-wrap">' +
                    '<button type="button" class="btn btn-success book-buy-button">' +
                       ' <i class="fa fa-shopping-cart" aria-hidden="true"></i> Buy' +
                    '</button>' +
               ' </div>'
            
            
            bookPriceWrapper.append("$");
            bookPriceWrapper.append(bookPrice);

            bookType.append(bookHeader);
            bookType.append(bookAuthor);
            bookType.append(bookIntro);
            bookType.append(bookPriceWrapper);
            bookType.append(bookBuyButton);

            book.append(bookType);
            bookWrapper.append(book);

            $(".books .row").append(bookWrapper);
        });
                
        $(".book").hover(function(){
            $(this).animate({backgroundSize: "120%"});      
            $(this).children("div").show();
            
        }, function() {
            $(this).animate({backgroundSize: "100%"});
            $(this).children("div").hide();
        });
        
        $(".book-buy-button").click(function () {  
            
            var buttonWrap = $(this).parent(".book-buy-button-wrap");
            var priceWrap = buttonWrap.siblings(".book-price-wrap");
            var price = priceWrap.children(".book-price").text();            
            var currentPrice = $("#price").text();
            
            var resultPrice = parseFloat(currentPrice) + parseFloat(price);
            
            $("#price").text(resultPrice.toFixed(2));            
        });
                
        
        $("#js").click(function () { 
            var cssWrap = $(".css").parent();
            var jsWrap = $(".js").parent();
                        
            cssWrap.hide();
            cssWrap.parent().removeClass("col-md-4");
            
            jsWrap.show();
            jsWrap.parent().addClass("col-md-4");
            
        });
        
        $("#css").click(function () { 
            var cssWrap = $(".css").parent();
            var jsWrap = $(".js").parent();
            
            jsWrap.hide();           
            jsWrap.parent().removeClass("col-md-4");        
            
            cssWrap.show();     
            cssWrap.parent().addClass("col-md-4");     
        });
        
        $("#all").click(function () { 
            var cssWrap = $(".css").parent();
            var jsWrap = $(".js").parent();
            
           
            jsWrap.parent().addClass("col-md-4");
            jsWrap.show();
            
            cssWrap.parent().addClass("col-md-4");     
            cssWrap.show();                       
        });        
        
    });
});
